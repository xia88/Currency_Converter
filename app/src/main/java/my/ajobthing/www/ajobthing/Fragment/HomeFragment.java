package my.ajobthing.www.ajobthing.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.provider.ContactsContract;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.OnTouch;
import butterknife.Unbinder;
import my.ajobthing.www.ajobthing.HistoryAdapter;
import my.ajobthing.www.ajobthing.HistoryModel;
import my.ajobthing.www.ajobthing.HistoryTouch;
import my.ajobthing.www.ajobthing.R;
import my.ajobthing.www.ajobthing.SpinnerAdapter;

/**
 * Created by flix on 12/16/17.
 */

public class HomeFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.base)
    Spinner fromSpinner;
    @BindView(R.id.to)
    Spinner toSpinner;
    @BindArray(R.array.country)
    String [] countryarray;
    @BindArray(R.array.currency)
    String [] currencyarray;
    @BindView(R.id.fromTV)
    EditText fromET;
    @BindView(R.id.toTV)
    EditText toET;
    @BindView(R.id.result)
    TextView result;
    @BindView(R.id.recycler_view)
    RecyclerView rv;
    @BindView(R.id.fragment_home_container)
    CoordinatorLayout coordinatorLayout;
    private final long DELAY = 750;
    SpinnerAdapter dataSpinner;
    SpinnerAdapter apiSpinner;
    DatabaseReference DatabaseUser;
    DatabaseReference History;
    FirebaseAuth firebaseAuth;
    SimpleDateFormat Timestamp;
    String FixedUrl;
    String [] DataCurrency;
    String [] DataValue;
    Trace myTrace;
    Timer timerfrom = new Timer();
    Timer timerto = new Timer();
    List<HistoryModel> HistoryModelL;
    HistoryAdapter historyAdapter ;
    TypedArray icon;
    LinearLayoutManager layoutmanager;
    ArrayList<String> KeyIndexData = new ArrayList<>();
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy / HH:mm:ss");
    String Key_Latest_Date = df.format(calendar.getTime());

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        myTrace.stop();
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myTrace = FirebasePerformance.getInstance().newTrace(HomeFragment.class.getSimpleName());
        myTrace.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView   = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder        = ButterKnife.bind(this, rootView);
        firebaseAuth    = FirebaseAuth.getInstance();
        icon            = getActivity().getResources().obtainTypedArray(R.array.flags);
        DatabaseUser    = FirebaseDatabase.getInstance().getReference("User").child(firebaseAuth.getCurrentUser().getUid());
        HistoryModelL   = new ArrayList<>();
        layoutmanager = new LinearLayoutManager(getActivity());
        layoutmanager.setReverseLayout(true);
        layoutmanager.setStackFromEnd(true);
        rv.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        rv.setLayoutManager(layoutmanager);
        historyAdapter = new HistoryAdapter(HistoryModelL);
        rv.setAdapter(historyAdapter);
        Timestamp       = new SimpleDateFormat("dd MM yyyy hh mm ss");
        dataSpinner     = new SpinnerAdapter(getActivity(),true,null,null);
        fromSpinner.setAdapter(dataSpinner);
        fromSpinner.setSelection(31);
        Bundle args = getArguments();
        DataCurrency = args.getStringArray("DataCurrency");
        DataValue = args.getStringArray("DataValue");
        apiSpinner = new SpinnerAdapter(getContext(),false, DataCurrency, DataValue);
        toSpinner.setAdapter(apiSpinner);
        toSpinner.setSelection(0);
        EnableInput(true);
        HistoryTouch.HistoryTouchListener historyTouchListener;
        historyTouchListener = new HistoryTouch.HistoryTouchListener() {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
                if (viewHolder instanceof HistoryAdapter.MyViewHolder) {
                    // get the removed item name to display it in snack bar
                    String name = HistoryModelL.get(viewHolder.getAdapterPosition()).getdate();
                    final HistoryModel deletedItem = HistoryModelL.get(viewHolder.getAdapterPosition());
                    final int deletedIndex = viewHolder.getAdapterPosition();
                    historyAdapter.removeItem(viewHolder.getAdapterPosition());
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, name + " removed from Database!", Snackbar.LENGTH_LONG);
                    snackbar.setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            historyAdapter.restoreItem(deletedItem, deletedIndex);
                        }
                    });
                    snackbar.setActionTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
                    snackbar.show();
                    snackbar.addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            DatabaseUser.child(KeyIndexData.get(deletedIndex)).removeValue();
                            KeyIndexData.remove(deletedIndex);
                        }

                        @Override
                        public void onShown(Snackbar snackbar) {

                        }
                    });
                }
            }
        };
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new HistoryTouch(0, ItemTouchHelper.LEFT, historyTouchListener);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rv);

        DatabaseUser.limitToLast(5).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                HistoryModel model = dataSnapshot.getValue(HistoryModel.class);
                if (Key_Latest_Date.compareTo(model.getdate()) < 0){
                    KeyIndexData.add(dataSnapshot.getKey());
                    HistoryModelL.add(model);
                    rv.scrollToPosition(HistoryModelL.size() - 1);
                    historyAdapter.notifyItemInserted(historyAdapter.getItemCount() -1);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        return rootView;
    }



    public void getApiData (final String Base, final String LastSelection){
        EnableInput(false);
        FixedUrl = "https://api.fixer.io/latest?base="+Base;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FixedUrl,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj  = new JSONObject(response);
                    DataCurrency = new String [obj.getJSONObject("rates").names().length()];
                    DataValue = new String [obj.getJSONObject("rates").names().length()];
                    for (int i = 0; i< obj.getJSONObject("rates").names().length() ; i++ ){
                        DataCurrency[i] = String.valueOf(obj.getJSONObject("rates").names().getString(i));
                        DataValue[i]    = String.valueOf(obj.getJSONObject("rates").get(obj.getJSONObject("rates").names().getString(i)));
                    };
                    apiSpinner      = new SpinnerAdapter(getContext(),false, DataCurrency, DataValue);
                    toSpinner.setAdapter(apiSpinner);
                    if (!Base.equals(LastSelection)){
                        toSpinner.setSelection(apiSpinner.getindexbycurrency(LastSelection));
                    }
                    else {
                        toSpinner.setSelection(0);
                    }
                    EnableInput(true);
                }catch (JSONException e) {
                            Toast.makeText(getContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), "Volley Error: maybe u internet is broken ", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }




    public void EnableInput (Boolean mode){
        toET.setEnabled(mode);
        fromET.setEnabled(mode);
        toSpinner.setEnabled(mode);
        fromSpinner.setEnabled(mode);
    }



    public String CurrencyFormat(String FormatC,float value){
        if (value > 0.001f){
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            format.setCurrency(Currency.getInstance(FormatC));
            format.setMaximumFractionDigits(3);
            return  format.format(value);
        }
        else {
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            format.setCurrency(Currency.getInstance(FormatC));
            format.setMaximumFractionDigits(9);
            return  format.format(value);
        }
    }

    public Float StringtoFloat (String number){
        Float convert = Float.parseFloat(number);
        return convert;
    }

    public void StoreData (final String FFrom, final String FTo, final String FPrice, final String FInput, final String FResult) {
        History = DatabaseUser.child(DatabaseUser.push().getKey());
        Calendar calendarInput = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy / HH:mm:ss");
        History.setValue(new HistoryModel(df.format(calendarInput.getTime()),FFrom,FTo,FPrice,FInput,FResult));
        result.setText(FResult);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnItemSelected(R.id.base)
    public void baseItemSelected(Spinner spinner, int position) {
            getApiData(dataSpinner.getCurrency(position), apiSpinner.getinputCurrency(toSpinner.getSelectedItemPosition()));
    }

    @OnItemSelected(R.id.to)
    public void fromItemSelected(Spinner spinner, int position) {
        if (toET.getText().length() >0){
            fromET.setText("");
            float Price     = StringtoFloat(apiSpinner.getinputValue(toSpinner.getSelectedItemPosition()));
            float NumberTo  = StringtoFloat(toET.getText().toString());
            String Currency = dataSpinner.getCurrency(fromSpinner.getSelectedItemPosition());
            result.setText(CurrencyFormat(Currency,NumberTo / Price));
            StoreData(apiSpinner.getinputCurrency(toSpinner.getSelectedItemPosition()),Currency,CurrencyFormat(Currency,1/Price),CurrencyFormat(apiSpinner.getCurrency(toSpinner.getSelectedItemPosition()),NumberTo),CurrencyFormat(Currency,NumberTo / Price));
        }
        if (fromET.getText().length() >0){
            toET.setText("");
            float Price     = StringtoFloat(apiSpinner.getinputValue(toSpinner.getSelectedItemPosition()));
            float NumberTo  = StringtoFloat(fromET.getText().toString());
            String Currency = apiSpinner.getinputCurrency(toSpinner.getSelectedItemPosition());
            result.setText(CurrencyFormat(Currency,NumberTo * Price));
            StoreData(dataSpinner.getCurrency(fromSpinner.getSelectedItemPosition()),Currency,CurrencyFormat(Currency,Price),CurrencyFormat(dataSpinner.getCurrency(fromSpinner.getSelectedItemPosition()),NumberTo),CurrencyFormat(Currency,NumberTo * Price));
        }
    }

    @OnTextChanged(value = R.id.toTV, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void toafterEditTextChanged(final Editable editable) {
        timerto.cancel();
        timerto = new Timer();
        timerto.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (toET.getText().length() >0){
                            fromET.setText("");
                            float Price     = StringtoFloat(apiSpinner.getinputValue(toSpinner.getSelectedItemPosition()));
                            float NumberTo  = StringtoFloat(editable.toString());
                            String Currency = dataSpinner.getCurrency(fromSpinner.getSelectedItemPosition());
                            StoreData(apiSpinner.getinputCurrency(toSpinner.getSelectedItemPosition()),Currency,CurrencyFormat(Currency,1/Price),CurrencyFormat(apiSpinner.getCurrency(toSpinner.getSelectedItemPosition()),NumberTo),CurrencyFormat(Currency,NumberTo / Price));
                        }
                    }
                });

            }
        }, DELAY);
    }

    @OnTextChanged(value = R.id.fromTV, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void FromafterEditTextChanged(final Editable editable) {
        timerfrom.cancel();
        timerfrom = new Timer();
        timerfrom.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (fromET.getText().length() >0){
                            toET.setText("");
                            float Price     = StringtoFloat(apiSpinner.getinputValue(toSpinner.getSelectedItemPosition()));
                            float NumberTo  = StringtoFloat(editable.toString());
                            String Currency = apiSpinner.getinputCurrency(toSpinner.getSelectedItemPosition());
                            StoreData(dataSpinner.getCurrency(fromSpinner.getSelectedItemPosition()),Currency,CurrencyFormat(Currency,Price),CurrencyFormat(dataSpinner.getCurrency(fromSpinner.getSelectedItemPosition()),NumberTo),CurrencyFormat(Currency,NumberTo * Price));
                        }
                    }
                });
            }
        }, DELAY);
    }



}
