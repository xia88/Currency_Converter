package my.ajobthing.www.ajobthing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by flix on 12/28/17.
 */

public class HistoryModel {
    String date,from,to,price,input,result;

    public HistoryModel() {
    }

    public String getdate() {
        return date;
    }

    public String getfrom() {
        return from;
    }

    public String getto() {
        return to;
    }

    public String getprice() {
        return price;
    }

    public String getinput() {
        return input;
    }

    public String getresult() {
        return result;
    }

    public HistoryModel(String date, String from, String to, String price, String input, String result) {
        this.date = date;
        this.from = from;
        this.to = to;
        this.price = price;
        this.input = input;
        this.result = result;
    }
}
