package my.ajobthing.www.ajobthing.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.ajobthing.www.ajobthing.R;


public class RegisterScreen extends AppCompatActivity {
    FirebaseAuth auth;
    @BindView(R.id.registerContainer)
    View registerContainer;
    @BindView(R.id.loginTextview)
    TextView login;
    String loginString = "Already Member? Login " + "<b>" + "Here" + "</b> ";
    @BindView(R.id.editEmail)
    EditText editemail;
    @BindView(R.id.editPassword)
    EditText editPassword;
    @BindView(R.id.editRePassword)
    EditText editRePassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.buttonRegister)
    Button buttonRegister;
    Trace myTrace;
    String [] DataCurrency;
    String [] DataValue;
    String FixedUrl;
    Bundle ApiData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);
        myTrace = FirebasePerformance.getInstance().newTrace(RegisterScreen.class.getSimpleName());
        myTrace.start();
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        login.setText(Html.fromHtml(loginString));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent nextactivity = new Intent(RegisterScreen.this, WelcomeScreen.class);
        startActivity(nextactivity);
        myTrace.stop();
        finish();
    }

    @OnClick(R.id.loginTextview)
    public void onLogin(View view) {
        Intent nextactivity = new Intent(RegisterScreen.this, LoginScreen.class);
        startActivity(nextactivity);
        myTrace.stop();
        finish();
    }

    @OnClick(R.id.buttonRegister)
    public void onRegister(View view){
        View focus = this.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        buttonRegister.setEnabled(false);
//        setSnackBar(registerContainer,"TEST","Test");
        if (isValidEmail(editemail.getText().toString()) && validatePassword()){
            progressBar.setVisibility(View.VISIBLE);
            auth.createUserWithEmailAndPassword(editemail.getText().toString(), editPassword.getText().toString())
                    .addOnCompleteListener(RegisterScreen.this, new OnCompleteListener<AuthResult>(){

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressBar.setVisibility(View.GONE);
                            if (!task.isSuccessful()) {
                                if (task.getException().getMessage().isEmpty()){
                                    setSnackBar("Failed : " + "Something Wrong");
                                }
                                else {
                                    setSnackBar("Failed : " + task.getException().getMessage().toString());
                                    Log.e("Firebase error", task.getException().getMessage().toString());
                                }
                            } else {
                                GoToHomeScreen();

                            }
                            buttonRegister.setEnabled(true);
                        }

                    });

        }
        buttonRegister.setEnabled(true);
    }



    private boolean isValidEmail(String email) {
        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true;
        }
        else {
            setSnackBar("Please Enter valid email address");
            return false;
        }

    }

    private boolean validatePassword() {
        if (editPassword.getText().toString().trim().isEmpty() || editPassword.getText().length()  < 7) {
            setSnackBar("Password min 8 Character");
            requestFocus(editPassword);
            return false;
        }

        else if (!editPassword.getText().toString().trim().equals(editRePassword.getText().toString())){
            setSnackBar("Password is not match");
            requestFocus(editPassword);
            return false;
        }
        else return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(registerContainer, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public void GoToHomeScreen (){
        FixedUrl = "https://api.fixer.io/latest";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FixedUrl,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj  = new JSONObject(response);
                    DataCurrency = new String [obj.getJSONObject("rates").names().length()];
                    DataValue = new String [obj.getJSONObject("rates").names().length()];
                    for (int i = 0; i< obj.getJSONObject("rates").names().length() ; i++ ){
                        DataCurrency[i] = String.valueOf(obj.getJSONObject("rates").names().getString(i));
                        DataValue[i]    = String.valueOf(obj.getJSONObject("rates").get(obj.getJSONObject("rates").names().getString(i)));
                    };
                    Intent nextactivity = new Intent(RegisterScreen.this, MainActivity.class);
                    ApiData   = new Bundle();
                    ApiData.putStringArray("DataCurrency",DataCurrency);
                    ApiData.putStringArray("DataValue",DataValue);
                    nextactivity.putExtras(ApiData);
                    startActivity(nextactivity);
                    myTrace.stop();
                    finish();
                }catch (JSONException e) {
                    Toast.makeText(RegisterScreen.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterScreen.this, "Fail get Data. Please Check your internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
