package my.ajobthing.www.ajobthing.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.ajobthing.www.ajobthing.R;
import my.ajobthing.www.ajobthing.SpinnerAdapter;

public class LoginScreen extends AppCompatActivity {
    FirebaseAuth auth;
    @BindView(R.id.loginContainer)
    View registerContainer;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.registerTextview)
    TextView register;
    String registerString   = "Not a Member? Sign Up  " + "<b>" + "Here" + "</b> ";
    String forgotString     = "Forgot Password ? " + "<b>" + "Here" + "</b> ";
    @BindView(R.id.editEmail)
    EditText editEmail;
    @BindView(R.id.editPassword)
    EditText editPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    @BindView(R.id.forgotPasswordTextview)
    TextView forgotPassword;
    Trace myTrace;
    String [] DataCurrency;
    String [] DataValue;
    String FixedUrl;
    Bundle ApiData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myTrace = FirebasePerformance.getInstance().newTrace(LoginScreen.class.getSimpleName());
        myTrace.start();
        auth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_login_screen);
        ButterKnife.bind(this);
        register.setText(Html.fromHtml(registerString));
        forgotPassword.setText(Html.fromHtml(forgotString));
    }

    @OnClick(R.id.registerTextview)
    public void onRegister(View view) {
        Intent nextactivity = new Intent(LoginScreen.this, RegisterScreen.class);
        startActivity(nextactivity);
        myTrace.stop();
        finish();
    }

    @OnClick(R.id.forgotPasswordTextview)
    public void onForgot(View view){
        LayoutInflater inflater = getLayoutInflater();
        final View dialogLayout = inflater.inflate(R.layout.dialog_forgot, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginScreen.this);
        final AlertDialog dialog = builder.create();
        dialog.setView(dialogLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ProgressBar progressBar = dialogLayout.findViewById(R.id.progressBar);
        final Button confirmationButton = dialogLayout.findViewById(R.id.confirmation_button);
        final TextView confirmationTextview = dialogLayout.findViewById(R.id.confirmation_textview);
        confirmationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (confirmationTextview.getText().length()>0){
                    progressBar.setVisibility(View.VISIBLE);
                    auth.sendPasswordResetEmail(confirmationTextview.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        setSnackBar("We have sent you instructions to reset your password!");
                                    } else {
                                        setSnackBar("Failed to send reset email!");
                                    }
                                    progressBar.setVisibility(View.GONE);
                                    dialog.dismiss();
                                }
                            });
                }
                else {

                    setSnackBar("Input your Email for reset");
                }
            }
        });
        dialog.show();
    }


    @OnClick(R.id.buttonLogin)
    public void onLogin(View view){
        buttonLogin.setEnabled(false);
        View focus = this.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (isValidEmail(editEmail.getText().toString()) && validatePassword()){
            progressBar.setVisibility(View.VISIBLE);
            if (auth.getCurrentUser() == null) {
                auth.signInWithEmailAndPassword(editEmail.getText().toString(), editPassword.getText().toString())
                        .addOnCompleteListener(LoginScreen.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    setSnackBar( task.getException().getMessage().toString());
                                } else {
                                    getApiData();
                                }
                            }
                        });
            }
        }
        buttonLogin.setEnabled(true);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent nextactivity = new Intent(LoginScreen.this, WelcomeScreen.class);
        startActivity(nextactivity);
        myTrace.stop();
        finish();
    }



    private boolean isValidEmail(String email) {
        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true;
        }
        else {
            setSnackBar("Please Enter valid email address");
            return false;
        }

    }

    private boolean validatePassword() {
        if (editPassword.getText().toString().trim().isEmpty() || editPassword.getText().length() <= 8) {
            setSnackBar("Password min 8 Character");
            requestFocus(editPassword);
            return false;
        }
        else return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(registerContainer, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public void getApiData (){
        FixedUrl = "https://api.fixer.io/latest";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FixedUrl,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj  = new JSONObject(response);
                    DataCurrency = new String [obj.getJSONObject("rates").names().length()];
                    DataValue = new String [obj.getJSONObject("rates").names().length()];
                    for (int i = 0; i< obj.getJSONObject("rates").names().length() ; i++ ){
                        DataCurrency[i] = String.valueOf(obj.getJSONObject("rates").names().getString(i));
                        DataValue[i]    = String.valueOf(obj.getJSONObject("rates").get(obj.getJSONObject("rates").names().getString(i)));
                    };
                    Intent nextactivity = new Intent(LoginScreen.this, MainActivity.class);
                    ApiData = new Bundle();
                    ApiData.putStringArray("DataCurrency",DataCurrency);
                    ApiData.putStringArray("DataValue",DataValue);
                    nextactivity.putExtras(ApiData);
                    startActivity(nextactivity);
                    myTrace.stop();
                    finish();
                }catch (JSONException e) {
                    Toast.makeText(LoginScreen.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginScreen.this, "Fail get Data. Please Check your internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
