package my.ajobthing.www.ajobthing.Fragment;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import my.ajobthing.www.ajobthing.HistoryAdapter;
import my.ajobthing.www.ajobthing.HistoryModel;
import my.ajobthing.www.ajobthing.R;

/**
 * Created by flix on 12/16/17.
 */

public class InformationFragment extends Fragment {

    Unbinder unbinder;
    Trace myTrace;
    @BindView(R.id.buttonfirebase)
    Button bdatabase;
    @BindView(R.id.buttoncrash)
    Button bcrash;
    @BindView(R.id.buttonauth)
    Button bauth;
    @BindView(R.id.buttonperformance)
    Button bperformance;
    @BindView(R.id.buttonaddtouch)
    Button baddtouch;
    @BindView(R.id.buttonbutterknife)
    Button bbutterknife;
    @BindView(R.id.buttonnavigation)
    Button bnavigation;
    @BindView(R.id.buttonspinner)
    Button bspinner;
    @BindView(R.id.buttonvolley)
    Button bvolley;
    @BindView(R.id.buttonanimation)
    Button banimation;
    @BindView(R.id.buttonhistory)
    Button  bhistory;

    @BindView(R.id.tvaddtouch)
    TextView tvaddtouch;
    @BindView(R.id.tvanimation)
    TextView tvanimation;
    @BindView(R.id.tvauth)
    TextView tvauth;
    @BindView(R.id.tvbutterknife)
    TextView tvbutterknife;
    @BindView(R.id.tvcrash)
    TextView tvcrash;
    @BindView(R.id.tvfirebase)
    TextView tvfirebase;
    @BindView(R.id.tvhistory)
    TextView tvhistory;
    @BindView(R.id.tvnav)
    TextView tvnav;
    @BindView(R.id.tvperformance)
    TextView tvperformance;
    @BindView(R.id.tvspinner)
    TextView tvspinner;
    @BindView(R.id.tvvolley)
    TextView tvvolley;

    @Override
    public void onDestroy() {
        super.onDestroy();
        myTrace.stop();
    }

    public InformationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myTrace = FirebasePerformance.getInstance().newTrace(InformationFragment.class.getSimpleName());
        myTrace.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_information, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonhistory)
    public void historyClick(View view){
        setVisible(tvhistory);
    }

    @OnClick(R.id.buttonfirebase)
    public void firebaseClick(View view){
        setVisible(tvfirebase);
    }

    @OnClick(R.id.buttonanimation)
    public void animationClick(View view){
        setVisible(tvanimation);
    }

    @OnClick(R.id.buttoncrash)
    public void crashClick(View view){
        setVisible(tvcrash);
    }

    @OnClick(R.id.buttonvolley)
    public void volleyClick(View view){
        setVisible(tvvolley);
    }

    @OnClick(R.id.buttonaddtouch)
    public void addTouchClick(View view){
        setVisible(tvaddtouch);
    }

    @OnClick(R.id.buttonbutterknife)
    public void butterknifeClick(View view){
        setVisible(tvbutterknife);
    }

    @OnClick(R.id.buttonspinner)
    public void spinnerClick(View view){
        setVisible(tvspinner);
    }

    @OnClick(R.id.buttonnavigation)
    public void navigationClick(View view){
        setVisible(tvnav);
    }

    @OnClick(R.id.buttonauth)
    public void authClick(View view){
        setVisible(tvauth);
    }

    @OnClick(R.id.buttonperformance)
    public void perfomClick(View view){
        setVisible(tvperformance);
    }

    public void setVisible (TextView textView){
        if (textView.getVisibility() == View.GONE){
            textView.setVisibility(View.VISIBLE);
        }
        else
        {
            textView.setVisibility(View.GONE);
        }
    }

}
