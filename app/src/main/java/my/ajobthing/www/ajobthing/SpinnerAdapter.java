package my.ajobthing.www.ajobthing;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.res.TypedArrayUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by flix on 12/21/17.
 */

public class SpinnerAdapter extends BaseAdapter implements Parcelable {

    @BindView(R.id.imageView)
    ImageView icon;
    @BindView(R.id.country)
    TextView countryTV;
    @BindView(R.id.currency)
    TextView currencyTV;
    private int mData;
    Context         context;
    LayoutInflater  inflter;
    Boolean         mode;
    TypedArray      icons  ;
    String          name[]   ;
    String          currency[];
    String          inputCurrency[];
    String          inputValue[];
    List<String> LIST_KEY = new ArrayList<>(Arrays.asList("AUD","BGN","BRL","CAD","CHF","CNY","CZK","DKK","GBP","HKD","HRK","HUF","IDR","ILS","INR","JPY","KRW","MXN","MYR","NOK","NZD","PHP","PLN","RON","RUB","SEK","SGD","THB","TRY","USD","ZAR","EUR"));


    public SpinnerAdapter(Context InputapplicationContext,Boolean mode, String InputCurrency[],String InputValue[]) {
            this.context        = InputapplicationContext;
            this.mode           = mode;
            this.icons          = InputapplicationContext.getResources().obtainTypedArray(R.array.flags);
            this.name           = InputapplicationContext.getResources().getStringArray(R.array.country);
            this.currency       = InputapplicationContext.getResources().getStringArray(R.array.currency);
            this.inputCurrency  = InputCurrency;
            this.inputValue     = InputValue;
            inflter = (LayoutInflater.from(InputapplicationContext));
    }

    @Override
    public int getCount() {
        if (mode){
            return 32;
        }
        else {
            return inputCurrency.length;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public String getinputCurrency(int i){
        return inputCurrency[i];
    }

    public String getinputValue(int i){
        return inputValue[i];
    }

    public String getName(int i){
        return name[i];
    }

    public int getindexbycurrency(String currency){
        int i = Arrays.asList(inputCurrency).indexOf(currency);
        return i;
    }

    public String getCurrency(int i){
        return currency[i];
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_flag, null);
        ButterKnife.bind(this, view);
        if (mode){
            icon.setImageResource(icons.getResourceId(i, -1));
            countryTV.setText(name[i]);
            currencyTV.setText(currency[i]);
        }
        else {
            int Key = LIST_KEY.indexOf(inputCurrency[i]);
            icon.setImageResource(icons.getResourceId(Key, -1));
            countryTV.setText(name[Key]);
            currencyTV.setText(currency[Key]);
        }
        return view;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    public static final Parcelable.Creator<SpinnerAdapter> CREATOR = new Parcelable.Creator<SpinnerAdapter>() {
        public SpinnerAdapter createFromParcel(Parcel in) {
            return new SpinnerAdapter(in);
        }

        public SpinnerAdapter[] newArray(int size) {
            return new SpinnerAdapter[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private SpinnerAdapter(Parcel in) {
        mData = in.readInt();
    }
}