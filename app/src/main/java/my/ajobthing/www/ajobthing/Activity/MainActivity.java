package my.ajobthing.www.ajobthing.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.Space;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.ajobthing.www.ajobthing.Fragment.AccountFragment;
import my.ajobthing.www.ajobthing.Fragment.HistoryFragment;
import my.ajobthing.www.ajobthing.Fragment.HomeFragment;
import my.ajobthing.www.ajobthing.Fragment.InformationFragment;
import my.ajobthing.www.ajobthing.R;
import my.ajobthing.www.ajobthing.SpinnerAdapter;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener authListener;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView((R.id.statusbarcolor))
    Space statusbarcolor;
    @BindView(R.id.main_drawer_layout)
    DrawerLayout maindrawerLayout;
    @BindView(R.id.home_button)
    LinearLayout homebutton;
    Trace myTrace;
    Fragment fragment;
    Bundle ApiData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myTrace = FirebasePerformance.getInstance().newTrace(MainActivity.class.getSimpleName());
        myTrace.start();
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        statusbarcolor.setMinimumHeight(getStatusBarHeight() );
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_more);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        maindrawerLayout.openDrawer(Gravity.START);
                    }
                }
        );
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(MainActivity.this, WelcomeScreen.class));
                    myTrace.stop();
                    finish();
                }
            }
        };
        ApiData = getIntent().getExtras();
        displayView("Home");
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @OnClick(R.id.home_button)
    public void onHome(View view) {
        displayView("Home");
    }

    @OnClick(R.id.history_button)
    public void onHistory(View view) {
        displayView("History");
    }

    @OnClick(R.id.button_account)
    public void onAccount(View view) {
        displayView("Account");
    }

    @OnClick(R.id.information_button)
    public void onInformation(View view) {
        displayView("Information");
    }

    @OnClick(R.id.button_logout)
    public void onLogout(View view) {
        auth.signOut();
        // this listener will be called when there is change in firebase user session
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }


    private void displayView(String title) {
        switch (title) {
            case "Home":
                fragment = new HomeFragment();
                fragment.setArguments(ApiData);
                break;
            case "Account":
                fragment = new AccountFragment();
                break;
            case "History":
                fragment = new HistoryFragment();
                break;
            case "Information":
                fragment = new InformationFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
        maindrawerLayout.closeDrawer(Gravity.START);
    }


    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(maindrawerLayout, snackTitle, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }
}
