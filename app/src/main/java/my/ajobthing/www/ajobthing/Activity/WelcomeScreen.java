package my.ajobthing.www.ajobthing.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

import butterknife.OnClick;
import my.ajobthing.www.ajobthing.R;

public class WelcomeScreen extends AppCompatActivity {
    @BindView(R.id.baling)
    ImageView baling;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.buttonLogin)
    Button loginButton;
    @BindView(R.id.buttonRegister)
    Button registerButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    RotateAnimation rotate;
    FirebaseAuth auth;
    Trace myTrace;
    String [] DataCurrency;
    String [] DataValue;
    String FixedUrl;
    Bundle ApiData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);
        myTrace = FirebasePerformance.getInstance().newTrace(WelcomeScreen.class.getSimpleName());
        myTrace.start();
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(5000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        baling.startAnimation(rotate);
        ObjectAnimator translation = ObjectAnimator.ofFloat(logo, "translationY", logo.getTop()-600);
        translation.setDuration(1000);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(translation);
        mAnimationSet.start();
        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (auth.getCurrentUser() != null){
                    GoToHomeScreen ();
                }
                else {
                    final AnimatorSet mAnimationSetMenu = new AnimatorSet();
                    ObjectAnimator fadeInlogin = ObjectAnimator.ofFloat(loginButton, "alpha", .3f, 1f);
                    fadeInlogin.setDuration(1000);
                    ObjectAnimator fadeInregister = ObjectAnimator.ofFloat(registerButton, "alpha", .3f, 1f);
                    fadeInregister.setDuration(1000);
                    mAnimationSetMenu.play(fadeInlogin).with(fadeInregister);
                    mAnimationSetMenu.start();
                }
            }
        });
    }


    @OnClick(R.id.buttonLogin)
    public void onLogin(View view) {
        Intent nextactivity = new Intent(WelcomeScreen.this, LoginScreen.class);
                startActivity(nextactivity);
                myTrace.stop();
                finish();
    }

    @OnClick(R.id.buttonRegister)
    public void onRegister(View view) {
        Intent nextactivity = new Intent(WelcomeScreen.this, RegisterScreen.class);
        startActivity(nextactivity);
        myTrace.stop();
        finish();
    }


    public void GoToHomeScreen (){
        FixedUrl = "https://api.fixer.io/latest";
        progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, FixedUrl,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj  = new JSONObject(response);
                    DataCurrency = new String [obj.getJSONObject("rates").names().length()];
                    DataValue = new String [obj.getJSONObject("rates").names().length()];
                    for (int i = 0; i< obj.getJSONObject("rates").names().length() ; i++ ){
                        DataCurrency[i] = String.valueOf(obj.getJSONObject("rates").names().getString(i));
                        DataValue[i]    = String.valueOf(obj.getJSONObject("rates").get(obj.getJSONObject("rates").names().getString(i)));
                        progressBar.setProgress(i);
                    };

                    Intent nextactivity = new Intent(WelcomeScreen.this, MainActivity.class);
                    ApiData = new Bundle();
                    ApiData.putStringArray("DataCurrency",DataCurrency);
                    ApiData.putStringArray("DataValue",DataValue);
                    nextactivity.putExtras(ApiData);
                    startActivity(nextactivity);
                    myTrace.stop();
                    finish();
                }catch (JSONException e) {
                    Toast.makeText(WelcomeScreen.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(WelcomeScreen.this, "Fail get Data. Please Check your internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
