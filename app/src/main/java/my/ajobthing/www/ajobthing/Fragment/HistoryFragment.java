package my.ajobthing.www.ajobthing.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import my.ajobthing.www.ajobthing.Activity.AccountEmail;
import my.ajobthing.www.ajobthing.Activity.WelcomeScreen;
import my.ajobthing.www.ajobthing.HistoryAdapter;
import my.ajobthing.www.ajobthing.HistoryModel;
import my.ajobthing.www.ajobthing.HistoryTouch;
import my.ajobthing.www.ajobthing.R;

/**
 * Created by flix on 12/16/17.
 */

public class HistoryFragment extends Fragment {
    Unbinder unbinder;
    Trace myTrace;
    DatabaseReference DatabaseUser;
    FirebaseAuth firebaseAuth;
    LinearLayoutManager layoutmanager;
    @BindView(R.id.fragment_history_container)
    CoordinatorLayout coordinatorLayout;
    ArrayList<String> KeyIndexData = new ArrayList<>();
    List<HistoryModel> HistoryModelL;
    HistoryAdapter historyAdapter ;
    @BindView(R.id.recycler_view)
    RecyclerView rv;
    Boolean isverify;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        myTrace.stop();
    }

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myTrace = FirebasePerformance.getInstance().newTrace(HistoryFragment.class.getSimpleName());
        myTrace.start();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        unbinder        = ButterKnife.bind(this, rootView);
        firebaseAuth    = firebaseAuth.getInstance();
        firebaseAuth.getCurrentUser().reload();
        isverify        = firebaseAuth.getCurrentUser().isEmailVerified();
        DatabaseUser    = FirebaseDatabase.getInstance().getReference("User").child(firebaseAuth.getCurrentUser().getUid());
        HistoryModelL   = new ArrayList<>();
        layoutmanager = new LinearLayoutManager(getContext());
        layoutmanager.setReverseLayout(true);
        layoutmanager.setStackFromEnd(true);
        rv.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        rv.setLayoutManager(layoutmanager);
        historyAdapter = new HistoryAdapter(HistoryModelL);
        if (!isverify){
            LayoutInflater dialoglayout = getLayoutInflater();
            final View dialogLayout = dialoglayout.inflate(R.layout.dialog_history, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            final AlertDialog dialog = builder.create();
            dialog.setView(dialogLayout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final ProgressBar progressBar = dialogLayout.findViewById(R.id.progressBar);
            final Button confirmationButton = dialogLayout.findViewById(R.id.confirmation_button);
            confirmationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    confirmationButton.setVisibility(View.GONE);
                    firebaseAuth.getCurrentUser().reload();
                    if (!firebaseAuth.getCurrentUser().isEmailVerified()){
                        firebaseAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                dialog.dismiss();
                                progressBar.setVisibility(View.GONE);
                                confirmationButton.setEnabled(true);
                                if (task.isSuccessful()) {
                                    setSnackBar("Verification email sent to " + firebaseAuth.getCurrentUser().getEmail());
                                } else {
                                    setSnackBar("Failed to send verification email");
                                }
                            }
                        });
                    }
                    else {
                        rv.setAdapter(historyAdapter);
                    }
                }
            });
            dialog.show();
        }
        else {
            rv.setAdapter(historyAdapter);
        }

        HistoryTouch.HistoryTouchListener historyTouchListener;
        historyTouchListener = new HistoryTouch.HistoryTouchListener() {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
                if (viewHolder instanceof HistoryAdapter.MyViewHolder) {
                    // get the removed item name to display it in snack bar
                    String name = HistoryModelL.get(viewHolder.getAdapterPosition()).getdate();
                    final HistoryModel deletedItem = HistoryModelL.get(viewHolder.getAdapterPosition());
                    final int deletedIndex = viewHolder.getAdapterPosition();
                    historyAdapter.removeItem(viewHolder.getAdapterPosition());
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, name + " removed from Database!", Snackbar.LENGTH_LONG);
                    snackbar.setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            historyAdapter.restoreItem(deletedItem, deletedIndex);
                        }
                    });
                    snackbar.setActionTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                    snackbar.show();
                    snackbar.addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            DatabaseUser.child(KeyIndexData.get(deletedIndex)).removeValue();
                            KeyIndexData.remove(deletedIndex);
                        }

                        @Override
                        public void onShown(Snackbar snackbar) {

                        }
                    });
                }
            }
        };
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new HistoryTouch(0, ItemTouchHelper.LEFT, historyTouchListener);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rv);

        DatabaseUser.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    HistoryModel model = dataSnapshot.getValue(HistoryModel.class);
                    KeyIndexData.add(dataSnapshot.getKey());
                    HistoryModelL.add(model);
                    rv.scrollToPosition(HistoryModelL.size() - 1);
                    historyAdapter.notifyItemInserted(historyAdapter.getItemCount() -1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }
}
