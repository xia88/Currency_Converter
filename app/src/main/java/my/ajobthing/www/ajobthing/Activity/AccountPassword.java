package my.ajobthing.www.ajobthing.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.Space;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.ajobthing.www.ajobthing.R;

public class AccountPassword extends AppCompatActivity {
    FirebaseAuth auth;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView((R.id.statusbarcolor))
    Space statusbarcolor;
    @BindView(R.id.newpassword)
    TextView newpassword;
    @BindView(R.id.repassword)
    TextView repassword;
    @BindView(R.id.container_password)
    LinearLayout container;
    @BindView(R.id.updatebutton)
    Button updateButton;
    Trace myTrace;


//    firebase.auth().currentUser.reauthenticateWithCredential(firebase.auth.EmailAuthProvider.credential(firebase.auth().currentUser.email, providedPassword);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_password);
        myTrace = FirebasePerformance.getInstance().newTrace(AccountPassword.class.getSimpleName());
        myTrace.start();
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        statusbarcolor.setMinimumHeight(getStatusBarHeight() );
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Password Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        myTrace.stop();
        finish();
    }

    @OnClick(R.id.updatebutton)
    public void onUpdate(){
        View focus = this.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }
        if (validatePassword()){
            LayoutInflater inflater = getLayoutInflater();
            final View dialogLayout = inflater.inflate(R.layout.dialog_confirmation, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(AccountPassword.this);
            final AlertDialog dialog = builder.create();
            dialog.setView(dialogLayout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final ProgressBar progressBar = dialogLayout.findViewById(R.id.progressBar);
            final Button confirmationButton = dialogLayout.findViewById(R.id.confirmation_button);
            final TextView confirmationTextview = dialogLayout.findViewById(R.id.confirmation_textview);
            confirmationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (confirmationTextview.getText().length()>0){
                        progressBar.setVisibility(View.VISIBLE);
                        AuthCredential credential = EmailAuthProvider.getCredential(auth.getCurrentUser().getEmail(), confirmationTextview.getText().toString());
                        auth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                dialog.dismiss();
                                if (task.isSuccessful()) {
                                    auth.getCurrentUser().updatePassword(newpassword.getText().toString().trim())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        auth.getCurrentUser().reload();
                                                        setSnackBar("Complete Update Password");
                                                    } else {
                                                        setSnackBar(task.getException().getMessage().toString());
                                                    }
                                                    dialog.dismiss();
                                                    progressBar.setVisibility(View.INVISIBLE);
                                                }
                                            });
                                }
                                else {
                                    dialog.dismiss();
                                    setSnackBar(task.getException().getMessage().toString());
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
                    }
                    else {
                        dialog.dismiss();
                        setSnackBar("Input your password for confirmation");
                    }
                }
            });
            dialog.show();
        }
    }

    private boolean validatePassword() {
        if (newpassword.getText().toString().trim().isEmpty() || newpassword.getText().length() <= 8) {
            setSnackBar("Password min 8 Character");
            requestFocus(newpassword);
            return false;
        }

        else if (!newpassword.getText().toString().trim().equals(repassword.getText().toString())){
            setSnackBar("Password is not match");
            requestFocus(newpassword);
            return false;
        }
        else return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(container, snackTitle, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }


}
