package my.ajobthing.www.ajobthing.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import my.ajobthing.www.ajobthing.Activity.AccountEmail;
import my.ajobthing.www.ajobthing.Activity.AccountPassword;
import my.ajobthing.www.ajobthing.Activity.WelcomeScreen;
import my.ajobthing.www.ajobthing.R;

/**
 * Created by flix on 12/16/17.
 */

public class AccountFragment extends Fragment {

    FirebaseAuth auth;
    Unbinder unbinder;
    Trace myTrace;
    @BindView(R.id.fragment_account_container)
    LinearLayout fragmentAccountContainer;
    @BindView(R.id.emailButton)
    LinearLayout emailButton;
    @BindView(R.id.passwordButton)
    LinearLayout passwordButton;
    @BindView(R.id.deleteButton)
    LinearLayout deleteButton;
//    @Nullable
//    @BindView(R.id.confirmation_textview)
//    TextView confirmationTextview;
//    @Nullable
//    @BindView(R.id.confirmation_button)
//    Button confirmationButton;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myTrace.stop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
        myTrace = FirebasePerformance.getInstance().newTrace(AccountFragment.class.getSimpleName());
        myTrace.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.deleteButton)
    public void deleteClick(View view){
        LayoutInflater inflater = getLayoutInflater();
        final View dialogLayout = inflater.inflate(R.layout.dialog_confirmation, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final AlertDialog dialog = builder.create();
        dialog.setView(dialogLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ProgressBar progressBar = dialogLayout.findViewById(R.id.progressBar);
        final Button confirmationButton = dialogLayout.findViewById(R.id.confirmation_button);
        final TextView confirmationTextview = dialogLayout.findViewById(R.id.confirmation_textview);
        confirmationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                confirmationButton.setEnabled(false);
                if (confirmationTextview.getText().length()>0){
                    AuthCredential credential = EmailAuthProvider.getCredential(auth.getCurrentUser().getEmail(), confirmationTextview.getText().toString());
                    auth.getCurrentUser().reauthenticate(credential)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    dialog.dismiss();
                                    if (task.isSuccessful()) {
                                        auth.getCurrentUser().delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Intent intent = new Intent(getActivity(), WelcomeScreen.class);
                                                    startActivity(intent);
                                                }
                                                else {
                                                    setSnackBar("Failed to delete your account!");
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        setSnackBar("Failed to delete your account!");
                                    }
                                }
                            });
                }
                else {
                    dialog.dismiss();
                    setSnackBar("Please fill password for confirmation delete account");
                }

            }
        });
        dialog.show();
    }

    @OnClick(R.id.emailButton)
    public void emailClick(View view){
        Intent intent = new Intent(getContext(), AccountEmail.class);
        startActivity(intent);
    }

    @OnClick(R.id.passwordButton)
    public void passwordClick(View view){
        Intent intent = new Intent(getContext(), AccountPassword.class);
        startActivity(intent);
    }


    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(fragmentAccountContainer, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

}
