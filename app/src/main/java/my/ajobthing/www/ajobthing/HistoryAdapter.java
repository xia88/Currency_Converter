package my.ajobthing.www.ajobthing;

import android.content.Context;
import android.content.res.TypedArray;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;

/**
 * Created by flix on 12/28/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private List<String> LIST_KEY = new ArrayList<>(Arrays.asList("AUD","BGN","BRL","CAD","CHF","CNY","CZK","DKK","GBP","HKD","HRK","HUF","IDR","ILS","INR","JPY","KRW","MXN","MYR","NOK","NZD","PHP","PLN","RON","RUB","SEK","SGD","THB","TRY","USD","ZAR","EUR"));
    private int flags[] = {R.drawable.flag_aud,R.drawable.flag_bgn,R.drawable.flag_brl,R.drawable.flag_cad,R.drawable.flag_chf,R.drawable.flag_cny,R.drawable.flag_czk,R.drawable.flag_dkk,R.drawable.flag_gbp,R.drawable.flag_hkd,R.drawable.flag_hrk,R.drawable.flag_huf,R.drawable.flag_idr,R.drawable.flag_ils,R.drawable.flag_inr,R.drawable.flag_jpy,R.drawable.flag_krw,R.drawable.flag_mxn,R.drawable.flag_myr,R.drawable.flag_nok,R.drawable.flag_nzd,R.drawable.flag_php,R.drawable.flag_pln,R.drawable.flag_ron,R.drawable.flag_rub,R.drawable.flag_sek,R.drawable.flag_sgd,R.drawable.flag_thb,R.drawable.flag_try,R.drawable.flag_usd,R.drawable.flag_zar,R.drawable.flag_europe};
    private List<HistoryModel> historyModels;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.delete_icon)
        ImageView delete;
        @BindView(R.id.fromFlag)
        ImageView fromflag;
        @BindView(R.id.toFlag)
        ImageView toFlag;
        @BindView(R.id.fromCurrecy)
        TextView fromCurrecy;
        @BindView(R.id.toCurrency)
        TextView toCurrency;
        @BindView(R.id.result)
        TextView result;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.input)
        TextView input;
        @BindView(R.id.view_background)
        RelativeLayout viewBackground;
        @BindView(R.id.view_foreground)
        LinearLayout viewForeground;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public HistoryAdapter(List<HistoryModel> historyModel) {
        this.historyModels = historyModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HistoryModel historyModel = historyModels.get(position);
        holder.fromCurrecy.setText(historyModel.getfrom());
        holder.toCurrency.setText(historyModel.getto());
        holder.result.setText("Result : "   +historyModel.getresult());
        holder.date.setText("Date : "       + historyModel.getdate());
        holder.price.setText("Price : "     + historyModel.getprice());
        holder.input.setText("Input : "     +historyModel.getinput());
        holder.fromflag.setImageResource(flags[LIST_KEY.indexOf(historyModel.getfrom())]);
        holder.toFlag.setImageResource(flags[LIST_KEY.indexOf(historyModel.getto())]);
    }

    @Override
    public int getItemCount() {
        return historyModels.size();
    }

    public void removeItem(int position) {
        historyModels.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(HistoryModel historyModel, int position) {
        historyModels.add(position, historyModel);
        notifyItemInserted(position);
    }

}
