package my.ajobthing.www.ajobthing.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.Space;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.ajobthing.www.ajobthing.R;

public class AccountEmail extends AppCompatActivity {
    FirebaseAuth auth;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView((R.id.statusbarcolor))
    Space statusbarcolor;
    @BindView(R.id.icon_verify)
    ImageView iconvVerify;
    @BindView(R.id.emailuser)
    TextView emailUser;
    @BindView(R.id.unverifiedtools)
    LinearLayout unverifiedtools;
    @BindView(R.id.verifyButton)
    Button verifyButton;
    @BindView(R.id.account_email_container)
    LinearLayout container;
    @BindView(R.id.progressBar_verified)
    ProgressBar progressBarverified;
    @BindView(R.id.progressBar_update)
    ProgressBar progressBarupdate;
    @BindView(R.id.updateButton)
    Button updateButton;
    @BindView(R.id.oldmail)
    TextView oldmail;
    @BindView(R.id.newmail)
    TextView newmail;
    Trace myTrace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_email);
        myTrace = FirebasePerformance.getInstance().newTrace(AccountEmail.class.getSimpleName());
        myTrace.start();
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        statusbarcolor.setMinimumHeight(getStatusBarHeight() );
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Email Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (auth.getCurrentUser() != null){
            auth.getCurrentUser().reload();
            emailUser.setText(auth.getCurrentUser().getEmail());
            if (!auth.getCurrentUser().isEmailVerified()){
                iconvVerify.setImageResource(R.drawable.icon_unverified);
            }
            else{
                auth.getCurrentUser().reload();
                iconvVerify.setImageResource(R.drawable.icon_verified);
                unverifiedtools.setVisibility(View.GONE);
            }
        }

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        myTrace.stop();
        finish();
    }

    @OnClick(R.id.verifyButton)
    public void onVerify(View view){
        progressBarverified.setVisibility(View.VISIBLE);
        verifyButton.setEnabled(false);
        if (auth.getCurrentUser() != null){
            auth.getCurrentUser().reload();
            if (auth.getCurrentUser().isEmailVerified()){
                iconvVerify.setImageResource(R.drawable.icon_verified);
                unverifiedtools.setVisibility(View.GONE);
                setSnackBar("Thanks your email already verified");
            }
            else {
                auth.getCurrentUser().sendEmailVerification().addOnCompleteListener(AccountEmail.this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressBarverified.setVisibility(View.GONE);
                        verifyButton.setEnabled(true);
                        if (task.isSuccessful()) {
                            setSnackBar("Verification email sent to " + auth.getCurrentUser().getEmail());
                        } else {
                            setSnackBar("Failed to send verification email");
                        }
                    }
                });
            }
        }
    }

    @OnClick(R.id.updateButton)
    public void  onUpdate(View view){
        View focus = this.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }
        if (isValidEmail(oldmail.getText().toString()) && isValidEmail(newmail.getText().toString())) {
            if (auth.getCurrentUser().getEmail().equals(oldmail.getText().toString().trim())){
                        LayoutInflater inflater = getLayoutInflater();
                        final View dialogLayout = inflater.inflate(R.layout.dialog_confirmation, null);
                        AlertDialog.Builder builder = new AlertDialog.Builder(AccountEmail.this);
                        final AlertDialog dialog = builder.create();
                        dialog.setView(dialogLayout);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        final ProgressBar progressBar = dialogLayout.findViewById(R.id.progressBar);
                        final Button confirmationButton = dialogLayout.findViewById(R.id.confirmation_button);
                        final TextView confirmationTextview = dialogLayout.findViewById(R.id.confirmation_textview);
                        confirmationButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (confirmationTextview.getText().length()>0){
                                    progressBar.setVisibility(View.VISIBLE);
                                    AuthCredential credential = EmailAuthProvider.getCredential(auth.getCurrentUser().getEmail(), confirmationTextview.getText().toString());
                                    auth.getCurrentUser().reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            dialog.dismiss();
                                            if (task.isSuccessful()) {
                                                auth.getCurrentUser().updateEmail(newmail.getText().toString().trim())
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    auth.getCurrentUser().reload();
                                                                    emailUser.setText(auth.getCurrentUser().getEmail());
                                                                    oldmail.setText("");
                                                                    newmail.setText("");
                                                                    if (!auth.getCurrentUser().isEmailVerified()){
                                                                        iconvVerify.setImageResource(R.drawable.icon_unverified);
                                                                    }
                                                                    else{
                                                                        auth.getCurrentUser().reload();
                                                                        iconvVerify.setImageResource(R.drawable.icon_verified);
                                                                        unverifiedtools.setVisibility(View.GONE);
                                                                    }
                                                                    setSnackBar("Email Update Complete");
                                                                } else {
                                                                    setSnackBar(task.getException().getMessage().toString());
                                                                }
                                                                dialog.dismiss();
                                                                progressBar.setVisibility(View.INVISIBLE);
                                                            }
                                                        });
                                            }
                                            else {
                                                dialog.dismiss();
                                                setSnackBar(task.getException().getMessage().toString());
                                                progressBar.setVisibility(View.INVISIBLE);
                                            }
                                        }
                                    });
                                }
                                else {
                                    dialog.dismiss();
                                    setSnackBar("Input your password for confirmation");
                                }
                            }
                        });
                dialog.show();
                if (focus != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);
                }
            }
            else {
                Log.e("FIREBASE", "Condition = FALSE");
                setSnackBar("Sorry, your old email is not match with current email!");
            }
        }
    }




    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(container, snackTitle, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }


    private boolean isValidEmail(String email) {
        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true;
        }
        else {
            setSnackBar("Please Enter valid email address");
            return false;
        }

    }
}
